<?php
  //constants of the paths in the application
define('WEBAPP_PATH', 'web_app/');
define('GRAPHICS',    'graphics/');
define('CSS',         'css/');
define('TPL',         'templates/');
define('MENU',        TPL.'main/menu/');
define('DOCBOOK',     TPL.'docbook/');
define('ADMIN',       TPL.'admin/');
define('SCRIPTS',     TPL.'scripts/');

define('XSLT',        'xsl_transform/');
define('WOBJ',        'webobjects/');

define('CONTENT',     'content/');
define('BOOKS',       CONTENT.'books/xml/');
define('SVN',         CONTENT.'books/svn/');
define('CACHE',       CONTENT.'books/cache/');

define('WORKSPACE',   'content/workspace/');
define('WS_BOOKS',    WORKSPACE.'xml/');
define('WS_CACHE',    WORKSPACE.'cache/');
?>
