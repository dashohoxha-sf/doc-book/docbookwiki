#!/bin/bash

### go to this dir
cd $(dirname $0)

### get book_id
if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  exit 1
fi
book_id=$1
lng=$2

### if language is given, delete the book only for this language
if [ "$lng" != "" ]
then
  ./del.sh $book_id $lng
  exit 0
fi

### otherwise delete the books with the given book_id (for any language)
for book in $(grep "^$book_id:" book_list)
do
  bookid=${book%:*}
  lng=${book#*:}
  if [ "$bookid" = "$book_id" ]
  then
    echo $book_id $lng
    ./del.sh $book_id $lng
  fi
done
