#!/bin/bash

### go to this dir
cd $(dirname $0)

### update each book in the list
for book in $(< book_list)
do
  book_id=${book%:*}
  lng=${book#*:}
  ./sync.sh $book_id $lng
done
