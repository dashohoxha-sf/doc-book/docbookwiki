#!/bin/bash
### update from svn the latest version of a book

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  echo "lng is: en, de, fr, it, al, etc. (default is en)"
  exit 1;
fi

book_id=$1
lng=${2:-en}

### read the name of the svn sandbox from svn_cfg.txt
. svn_cfg.txt

### variables
dir=$(pwd)
xml_file=$svn_dir/${book_id}_${lng}.xml
media=$svn_dir/media/$book_id/$lng/
book_dir=../books/xml/$book_id/$lng/
ws_book_dir=../workspace/xml/$book_id/$lng/
tmp_dir=../explode/tmp/$book_id
svn_url=$(svn info $book_dir | grep '^URL: ' | cut -d' ' -f2)

### update xml_file
echo "Updating  '$xml_file'"
svn update $xml_file

### explode in $tmp_dir
../explode/explode.sh SVN/$xml_file

### update media files
echo "Updating  '$media'"
svn update $media
cd $media
find . -name .svn -prune -o -type f -print \
  |  xargs tar cfz $dir/media.tgz  2> /dev/null
cd $dir

### copy media files in $tmp_dir
if [ -f media.tgz ]
then
  tar xfz media.tgz -C $tmp_dir
  rm media.tgz
fi

### commit in svn the version of book that is in $tmp_dir
echo "Updating '$book_dir'"
./svn_load_dirs.pl -wc $book_dir  $svn_url/ .  $tmp_dir

### clean $tmp_dir
rm -rf $tmp_dir

### update in the workspace
echo "Updating  '$ws_book_dir'"
svn update -q $ws_book_dir

### refresh cache files in books/ and workspace/
echo "Refreshing '../books/cache/$book_id/$lng/'"
../cache/cache.sh $book_id $lng 'books'
echo "Refreshing '../workspace/cache/$book_id/$lng/'"
../cache/cache.sh $book_id $lng 'workspace'


