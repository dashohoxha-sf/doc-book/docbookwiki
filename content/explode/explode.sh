#!/bin/bash
### Explodes an XML/DocBook file into chunks of xml files
### according to chapters, sections, etc. Creates also
### the file index.xml which contains the structure of
### the sections, their id-s and their paths.
### The output is placed in the directory explode/tmp/book_id/

### go to the 'content/' dir
cd $(dirname $0)/..

### check parameters
if [ "$1" = "" ]
then
  echo "
Usage: $0 file.xml [book_id] [lng]

Explodes an XML/DocBook file into chunks of xml files according
to chapters, sections, etc. Creates also the file index.xml
which contains the structure of the sections, their id-s and their paths.
The output is placed in the directory 'explode/tmp/book_id/' .

Param file.xml is the DocBook document (book or article) to be exploded.

Param book-id is the id of the book; if missing, it is taken from the
      attribute 'id' of the root element (book or article) of file.xml.

Param lng can be: en, en_US, en_US.UTF-8, sq_AL, etc.; if missing it is
      taken from the attribute 'lang' of file.xml (default is en).

Important: the path of file.xml should be either absolute,
           or relative to the 'content/' directory.
"
  exit 1
fi

### get parameters
original_xml_file=$1
book_id=$2
lng=$3

### check that the original_xml_file does exist
if [ ! -f $original_xml_file ]
then
  echo "$0: Error: file not found: $original_xml_file"
  echo "Important: the path of the file.xml should be either absolute,"
  echo "           or relative to the 'content/' directory."
  exit 2
fi

### xslt path
xslt=../xsl_transform/explode

### if book_id and lng are not given, get them from the attributes
### id and lang of the root element (book or article) of the xml file
if [ "$book_id" = "" ]
then
  book_id=$(xsltproc $xslt/get_id.xsl $original_xml_file)
  lng=$(xsltproc $xslt/get_lang.xsl $original_xml_file)
  lng=${lng:-en}  #default is english
fi

### if book_id is still undefined, stop processing
if [ "$book_id" = "" ]
then
  echo "$0: Error: book_id is undefined."
  exit 3;
fi

echo "Exploding '$book_id/$lng' $original_xml_file"

tmp_dir=explode/tmp/$book_id
rm -rf $tmp_dir
mkdir -p $tmp_dir
xml_file=${tmp_dir}.xml

### pre-process for extracting <![CDATA[...]]> etc.
cd explode/
if   [ "${original_xml_file:0:1}" = "/" ]  # it is an absolute path
then preprocess_xml_file=$original_xml_file
else preprocess_xml_file=../$original_xml_file
fi
./pre_process.php tmp/$book_id $preprocess_xml_file
cd ..

### preprocess the xml_file to make it satisfy the DocBookWiki conventions
### (e.g. <sectX> tags are replaced by <section> tags)
xsltproc -o $xml_file $xslt/pre-process.xsl $xml_file

### write the files 'content.xml' for each section
xsltproc -o $tmp_dir/unused.ignore \
         --stringparam id "$book_id" --stringparam lang "$lng" \
         $xslt/content-xml.xsl $xml_file

### create the file 'index.xml'
xsltproc -o $tmp_dir/index.xml \
         --stringparam id "$book_id" --stringparam lang "$lng" \
         $xslt/index-xml.xsl $xml_file

### post-process the xml chunks in order to put back <![CDATA[...]]> etc.
cd explode/
./post_process.php tmp/$book_id
cd ..

### clean intermediate files
rm $tmp_dir/{cdata.txt,comments.txt} $xml_file
