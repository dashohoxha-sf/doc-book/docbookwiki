<?xml version="1.0" encoding="utf-8" standalone="no"?>

<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
                      "http://docbook.org/xml/4.2/docbookx.dtd">

<article id="install_exim">
  <title>Installing Exim Mail Service</title>
  <para/>
  <articleinfo id="INFO">
    <author>
      <firstname>Dashamir</firstname>
      <surname>Hoxha</surname>
      <affiliation>
        <orgname>
          <ulink url="http://www.inima.al">INIMA</ulink>
        </orgname>
        <address>
          <email>dhoxha@inima.al</email>
        </address>
      </affiliation>
    </author>
    <abstract>
      <para>This article describes the installation and configuration of exim as an alternative MTA to sendmail. It has also support for virtual email domains and for webmail (accessing mailboxes from the web).</para>
    </abstract>
    <legalnotice>
      <para>Copyright (C) 2004 Dashamir Hoxha. Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.1 or any later version published by the Free Software Foundation; with no Invariant Sections, with no Front-Cover Texts, and with no Back-Cover Texts. A copy of the license is included in the section entitled "GNU Free Documentation License."</para>
    </legalnotice>
  </articleinfo>
  <section id="compilation">
    <title>Getting and Compiling Exim</title>
    <orderedlist numeration="arabic" inheritnum="ignore" continuation="restarts">
      <listitem>
        <para>Get the latest version of exim from <ulink url="http://www.exim.org/mirrors.html"/> , e.g.: 
      <screen>
<prompt>bash$</prompt> <command>wget ftp://ftp.csx.cam.ac.uk/pub/software/email/exim/exim4/exim-4.42.tar.gz .</command>
</screen>
Get also the latest docs, e.g.: 
      <screen>
<prompt>bash$</prompt> <command>wget ftp://ftp.csx.cam.ac.uk/pub/software/email/exim/exim4/exim-html-4.30.tar.gz .</command>
<prompt>bash$</prompt> <command>wget ftp://ftp.csx.cam.ac.uk/pub/software/email/exim/exim4/exim-pdf-4.30.tar.gz .</command>
</screen></para>
      </listitem>
      <listitem>
        <para>Extract it in <filename>/usr/local/src/</filename> : 
      <screen>
<prompt>bash#</prompt> <command>mv exim-*.tar.gz /usr/local/src/tgz/</command>
<prompt>bash#</prompt> <command>cd /usr/local/src/</command>
<prompt>bash#</prompt> <command>tar xfz tgz/exim-4.42.tar.gz</command>
<prompt>bash#</prompt> <command>cd exim-4.42/</command>
</screen></para>
      </listitem>
      <listitem>
        <para>Create <filename>Local/Makefile</filename> and modify it: 
      <screen><prompt>bash#</prompt> <command>cp src/EDITME Local/Makefile</command>
<prompt>bash#</prompt> <command>vi Local/Makefile</command></screen><programlisting>BIN_DIRECTORY=/usr/local/exim/bin
CONFIGURE_FILE=/usr/local/exim/etc/exim.conf
EXIM_USER=ref:mail
SPOOL_DIRECTORY=/var/spool/exim
SUPPORT_MAILDIR=yes
# EXIM_MONITOR=eximon.bin
LOG_FILE_PATH=/var/log/exim_%s.log
USE_TCP_WRAPPERS=yes
EXTRALIBS_EXIM=-lwrap -ldl</programlisting></para>
      </listitem>
      <listitem>
        <para>Make sure that <emphasis>db4</emphasis> and <emphasis>db4-devel</emphasis> packages are installed and install them if they aren't: 
      <screen><prompt>bash$</prompt> <command>rpm -q db4</command>
<prompt>bash$</prompt> <command>rpm -q db4-devel</command>
<prompt>bash#</prompt> <command>rpm -Uhv db4-....rpm</command>
<prompt>bash#</prompt> <command>rpm -Uhv db4-devel-....rpm</command></screen></para>
      </listitem>
      <listitem>
        <para>Compile and install: 
      <screen><prompt>bash$</prompt> <command>make</command>
<prompt>bash#</prompt> <command>make install</command></screen></para>
      </listitem>
    </orderedlist>
  </section>
  <section id="configuration">
    <title>Exim Configuration</title>
    <para>Edit the configuration file <filename>/usr/local/exim/exim.conf</filename> :
<screen>
<prompt>bash#</prompt> <command>vi /usr/local/exim/exim.conf</command>
</screen>
and make the modifications described in the following sections.</para>
    <section id="main_cfg">
      <title>Main (General) Configurations</title>
      <para>Modify these parameters at <filename>/usr/local/exim/etc/exim.conf</filename> at the MAIN CONFIGURATION SETTINGS section: 
    <programlisting><![CDATA[domainlist local_domains = gateway.inima.al : dns3.inima.al
domainlist relay_to_domains = inima.al : *.inima.al : akad.edu.al
hostlist   relay_from_hosts = 127.0.0.1 : 192.168.240.0/20 : 193.254.1.0/24
]]></programlisting><emphasis>relay_to_domains</emphasis> are the domains for which exim accepts email and <emphasis>relay_from_hosts</emphasis> are the hosts/networks which can send email using exim. </para>
      <para>Add these general parameters before the ACL CONFIGURATION section: 
    <programlisting><![CDATA[# Who to send a mail to when a message is frozen
freeze_tell = postmaster

# Redundant pairs of angle brackets around addresses are removed
# Default is false
strip_excess_angle_brackets = true

# Ignore a trailing dot at the end of a domain in an address
# Default is false
strip_trailing_dot = true

# the code used to create the cert and key:
# openssl req -x509 -newkey rsa:1024 -keyout /usr/local/exim/ssl/exim.key \
#         -out /usr/local/exim/ssl/exim.cert -days 9999 -nodes
# SSL/TLS cert and key
#tls_certificate = /usr/local/exim/ssl/exim.cert
#tls_privatekey = /usr/local/exim/ssl/exim.key
#tls_advertise_hosts = *
#auth_over_tls_hosts = *

]]></programlisting></para>
    </section>
    <section id="acl_cfg">
      <title>ACL Configuration</title>
      <para>These access rules provide some protection against spam and void messages, (e.g. messages sent to a user that does not exist at inima.al). </para>
      <para>Add these rules after <emphasis>require verify = sender</emphasis> : 
    <programlisting><![CDATA[
  # Do not accept HELO/EHLO from hosts using our IP(s) in HELO
  # Could exclude internal IPs, but they should never HELO with our inet IP
  # Remember to update file if IP(s) change!
  deny message     = Forged IP in HELO.
       log_message = HELO is our IP
       condition   = ${lookup {$sender_helo_name} \
                     lsearch{/usr/local/exim/etc/our_inet_ips.txt} \
                     {yes}{no}}

  # Deny unless the sender address can be verified.
  deny    message = From email address must be valid
          # do not check address for lists or bounces
          # or people in our company contact database
          #senders = ^.*-request@.*
          # do not check for DSN-ignorant domains
          # iow those that don't accept MAIL FROM:<>
          !verify  = sender/defer_ok

  # deny if the domain is inima.al but the local_part is not in the
  # list of users (users.inima.al)
  deny domains = inima.al
       local_parts = !lsearch;/usr/local/exim/etc/users.inima.al
]]></programlisting></para>
      <para>Create the file <filename>/usr/local/exim/etc/our_inet_ips.txt</filename> with all the IP numbers of the server: 
    <programlisting>80.90.82.73
193.254.1.195
192.168.251.195</programlisting></para>
      <para>Create the file <filename>/usr/local/exim/etc/users.inima.al</filename> with all the users of the domain inima.al (users of the server hpe25.inima.al): 
    <programlisting>nfra
gbeq
dhoxha
agor
enal
...</programlisting></para>
    </section>
    <section id="routers_cfg">
      <title>Routers Configuration</title>
      <para>In the ROUTERS CONFIGURATION section add these routers at the beginning, after <emphasis>begin routers</emphasis> : 
    <programlisting><![CDATA[# This router forwards all the emails for the domain inima.al
# to the server hpe25.inima.al (without any DNS lookup, MX records etc.)
special:
  driver = manualroute
  transport = remote_smtp
  route_list = inima.al hpe25.inima.al
]]></programlisting></para>
      <para>Also uncomment <emphasis>allow filter</emphasis> at <emphasis>userforward:</emphasis> router. 
    <programlisting><![CDATA[userforward:
  driver = redirect
  check_local_user
# local_part_suffix = +* : -*
# local_part_suffix_optional
  file = $home/.forward
  allow_filter
  no_verify
  no_expn
  check_ancestor
  file_transport = address_file
  pipe_transport = address_pipe
  reply_transport = address_reply
]]></programlisting></para>
    </section>
    <section id="transports_cfg">
      <title>Transports Configuration</title>
      <para>At the TRANSPORTS CONFIGURATION section add these transports: 
    <programlisting><![CDATA[local_delivery:
  driver = appendfile
  file = /var/mail/$local_part
  delivery_date_add
  envelope_to_add
  return_path_add
  group = mail       # uncomment these two lines
  mode = 0660
]]></programlisting></para>
    </section>
  </section>
  <section id="filter-cfg">
    <title>Filter Configuration</title>
    <orderedlist numeration="arabic" inheritnum="ignore" continuation="restarts">
      <listitem>
        <para>Add these parameters at <filename>/usr/local/exim/etc/exim.conf</filename> , before the ACL CONFIGURATION section: 
      <programlisting><![CDATA[# specify the system filter file
system_filter = /usr/local/exim/etc/exim.filter
system_filter_user = mail
system_filter_group = mail
]]></programlisting></para>
      </listitem>
      <listitem>
        <para>Create the system filter file <filename>/usr/local/exim/etc/exim.filter</filename> with this content: 
      <programlisting><![CDATA[# Exim filter  <-- do not remove it, it is required, it is not a comment
                                                                                
### throw away junk or bulk
if
  $h_precedence: is "junk" or
  $h_precedence: is "bulk"
then
  seen finish
endif
                                                                                
### forward mrtg errors to dhoxha, nfra, agor and nresulaj
if
  $h_from: contains "root@" and
  $h_subject: contains "/usr/bin/mrtg"
then
  deliver nfra@inima.al
  deliver dhoxha@inima.al
endif
                                                                                
if
  $h_from: contains "MAILER-DAEMON@hpe25.inima.al"
then
  deliver nfra@inima.al
endif
]]></programlisting></para>
      </listitem>
      <listitem>
        <para>Create a test message, like this: 
      <programlisting>From root@localhost Sat Mar 27 18:31:31 2004
Date: Thu, 29 Apr 2004 10:50:29 +0200
To: postmaster
From: MAILER-DAEMON@hpe25.inima.al
Subject: test

test message

</programlisting>
and test the system filter file: 
      <screen><prompt>bash$</prompt> <command>bin/exim -bF exim.filter &lt; test.msg</command>
<prompt>bash$</prompt> <command>bin/exim -v -bF exim.filter &lt; test.msg</command></screen></para>
      </listitem>
      <listitem>
        <para>Restart exim: 
      <screen><prompt>bash#</prompt> <command>/sbin/service exim restart</command></screen></para>
      </listitem>
    </orderedlist>
  </section>
  <section id="virtual-domains">
    <title>Adding Support for Virtual Domains</title>
    <para/>
    <section id="virt-domains-hosts">
      <title>Domains and Hosts</title>
      <para>Modify these parameters at <filename>/usr/local/exim/etc/exim.conf</filename> at the MAIN CONFIGURATION SETTINGS section, after <emphasis>primary_hostname</emphasis> : 
    <programlisting>domainlist local_domains = linux.inima.al : lsearch;/etc/virtual/domains
domainlist relay_to_domains = localhost : lsearch;/etc/virtual/domains
hostlist   relay_from_hosts = net-lsearch;/etc/virtual/pophosts : 127.0.0.1
</programlisting></para>
      <itemizedlist>
        <listitem>
          <para><emphasis>local_domains</emphasis> are the domains for which <emphasis>exim</emphasis> does not forward the emails to other servers, but keeps them for local delivery (to the local mailboxes). </para>
        </listitem>
        <listitem>
          <para><emphasis>relay_to_domains</emphasis> are the domains for which exim accepts emails for delivery (they can be delivered locally or forwarded to other MTA-s). </para>
        </listitem>
        <listitem>
          <para><emphasis>relay_from_hosts</emphasis> are the hosts/networks which are allowed to send email through this mail server. </para>
        </listitem>
        <listitem>
          <para><filename>/etc/virtual/</filename> is a directory that contains the configuration of the virtual domains (in our set-up it is actually a link to <filename>/usr/local/exim/virtual/</filename> . </para>
        </listitem>
        <listitem>
          <para><filename>/etc/virtual/domains</filename> is a file that contains a list of the virtual domains which are supportet by this mailserver, one in a line. </para>
        </listitem>
        <listitem>
          <para><filename>/etc/virtual/pophosts</filename> is a file that contains a list of IP-s or networks, one in each line, which are allowed to send email using this mailserver as SMTP. This is in order to prevent spammers from exploiting our mailserver for sending spam. </para>
        </listitem>
      </itemizedlist>
    </section>
    <section id="virt-routers">
      <title>Routers</title>
      <para>Add the following routers for handling the addresses in the local virtual domains: 
    <programlisting>
# The remaining routers handle addresses in the local domain(s).

domain_filter:
  driver = redirect
  allow_filter
  no_check_local_user
  user = "mail"
  file = /etc/virtual/${domain}/filter
  file_transport = address_file
  pipe_transport = virtual_address_pipe
  retry_use_local_part
  no_verify

uservacation:
   driver = accept
   condition = ${lookup{$local_part} lsearch {/etc/virtual/${domain}/vacation.conf}{yes}{no}}
   require_files = /etc/virtual/${domain}/reply/${local_part}.msg
   transport = uservacation
   unseen

userautoreply:
   driver = accept
   condition = ${lookup{$local_part} lsearch {/etc/virtual/${domain}/autoresponder.conf}{yes}{no}}
   require_files = /etc/virtual/${domain}/reply/${local_part}.msg
   transport = userautoreply

virtual_aliases_nostar:
  driver = redirect
  allow_defer
  allow_fail
  data = ${if exists{/etc/virtual/${domain}/aliases}{${lookup{$local_part}lsearch{/etc/virtual/${domain}/aliases}}}}
  file_transport = address_file
  group = mail
  pipe_transport = virtual_address_pipe
  retry_use_local_part
  unseen
  #include_domain = true

virtual_user:
  driver = accept
  condition = ${if eq {}{${if exists{/etc/virtual/${domain}/passwd}{${lookup{$local_part}lsearch{/etc/virtual/${domain}/passwd}}}}}{no}{yes}}
  domains = lsearch;/etc/virtual/domainowners
  group = mail
  retry_use_local_part
  transport = virtual_localdelivery

virtual_aliases:
  driver = redirect
  allow_defer
  allow_fail
  data = ${if exists{/etc/virtual/$domain/aliases}{${lookup{$local_part}lsearch*{/etc/virtual/$domain/aliases}}}}
  file_transport = address_file
  group = mail
  pipe_transport = virtual_address_pipe
  retry_use_local_part
  #include_domain = true
</programlisting></para>
      <para>Configuration of mailboxes, filters, passwords, etc. for a virtual domain are stored in the directory <filename>/etc/virtual/domain-name/</filename> . These includes: 
    <itemizedlist><listitem><para>the password file <filename>passwd</filename> , which is also a list of mailboxes for this domain </para></listitem><listitem><para>the <filename>aliases</filename> file, for ridirections to other email addresses </para></listitem><listitem><para>the <filename>filter</filename> file, which contains the filter rules (if any) for the domain </para></listitem><listitem><para>if the <emphasis>username</emphasis> is listed in the file <filename>vacation.conf</filename> or <filename>autoresponder.conf</filename>, then the message <filename>reply/username.msg</filename> (if exists) will be sent automatically as a reply to the message </para></listitem></itemizedlist></para>
    </section>
    <section id="virt-transports">
      <title>Transports</title>
      <para>In the TRANSPORTS CONFIGURATION section of <filename>/usr/local/exim/etc/exim.conf</filename> , add these transports for handling virtual domain delivery. The order of the transports is not important, so it doesn't matter whether they are added at the beginning or at the end of the section. </para>
      <programlisting>
        <![CDATA[
## for delivering virtual domains to their own mail spool
virtual_localdelivery:
  driver = appendfile
  create_directory
  delivery_date_add
  directory_mode = 700
  envelope_to_add
  file = /var/spool/virtual/${domain}/${local_part}
  group = mail
  mode = 660
  return_path_add
  user = "${lookup{$domain}lsearch*{/etc/virtual/domainowners}{$value}}"
  quota = ${if exists{/etc/virtual/${domain}/quota}{${lookup{$local_part}lsearch*{/etc/virtual/${domain}/quota}{$value}{0}}}{0}}

## vacation transport
uservacation:
  driver = autoreply
  file = /etc/virtual/${domain}/reply/${local_part}.msg
  from = "${local_part}@${domain}"
  log = /etc/virtual/${domain}/reply/${local_part}.log
  no_return_message
  subject = "${if def:h_Subject: {Autoreply: $h_Subject:} {I am on vacation}}"
  text = "\
        ------                                                           ------\n\n\
        This message was automatically generated by email software\n\
        The delivery of your message has not been affected.\n\n\
        ------                                                           ------\n\n"
  to = "${sender_address}"
  user = mail
        #once = /etc/virtual/${domain}/reply/${local_part}.once

userautoreply:
  driver = autoreply
  bcc = ${lookup{${local_part}} lsearch {/etc/virtual/${domain}/autoresponder.conf}{$value}}
  file = /etc/virtual/${domain}/reply/${local_part}.msg
  from = "${local_part}@${domain}"
  log = /etc/virtual/${domain}/reply/${local_part}.log
  no_return_message
  subject = "${if def:h_Subject: {Autoreply: $h_Subject:} {Autoreply Message}}"
  to = "${sender_address}"
  user = mail
  #once = /etc/virtual/${domain}/reply/${local_part}.once
]]>
      </programlisting>
      <para>As can be seen in <emphasis>virtual_localdelivery</emphasis> , the mailbox of a user is configured to be in <filename>/var/spool/virtual/domain.name/username</filename> . The directory should exist and have proper permissions. </para>
    </section>
    <section id="virt-manage">
      <title>Add/Delete</title>
      <para>Adding a virtual domain is done by the script <filename>/usr/local/exim/scripts/add-domain.sh</filename> : 
    <programlisting><![CDATA[#!/bin/bash
### add a new email domain

if [ "$1" = "" ]
then
  echo "Usage: $0 domain-name"
  echo "Example: $0 example.domain.org"
  exit 1
fi

domain=$1
virtual_dir='/etc/virtual'

### appens the new domain in the list of domains and domainowners
echo "$domain" >> $virtual_dir/domains
echo "$domain : mail" >> $virtual_dir/domainowners

### create the configuration files (passwds, aliases, etc.)
cp -a $virtual_dir/{skeleton-domain.example,$domain}

### create the spool directory (where will be stored the emails)
spool_dir="/var/spool/virtual/$domain"
mkdir $spool_dir
chown mail.mail $spool_dir
chmod 770 $spool_dir
]]></programlisting></para>
      <para>Deleting a virtual domain is done by the script <filename>/usr/local/exim/scripts/del-domain.sh</filename> : 
    <programlisting><![CDATA[#!/bin/bash
### delete a new email domain

if [ "$1" = "" ]
then
  echo "Usage: $0 domain-name"
  echo "Example: $0 example.domain.org"
  exit 1
fi

domain=$1
virtual_dir='/etc/virtual'

### remove the domain from the list of domains and domainowners
sed "/^$domain/d" -i $virtual_dir/domains
sed "/^$domain : /d" -i $virtual_dir/domainowners

### remove the configuration files
mv $virtual_dir/$domain $virtual_dir/deleted/

### remove the spool directory
spool_dir="/var/spool/virtual"
mv $spool_dir/$domain $spool_dir/deleted/
]]></programlisting></para>
    </section>
    <section id="vm-pop3d">
      <title>Virtual Mail POP3D</title>
      <para>The mail clients can access email messages in the mailserver using the IMAP or POP protocols. However not all of them support virtual emails. I will describe here the installation of a POP server that supports virtual email domains. It is <ulink url="http://www.reedmedia.net/software/virtualmail-pop3d/">vm-pop3d</ulink> . </para>
      <para>The compilation and installation is very easy: </para>
      <orderedlist numeration="arabic" inheritnum="ignore" continuation="restarts">
        <listitem>
          <para>Download the latest version from the page above. </para>
        </listitem>
        <listitem>
          <para>Unpack, compile and install it: 
      <screen><prompt>bash$</prompt> <command>./configure</command>
<prompt>bash$</prompt> <command>make</command>
<prompt>bash#</prompt> <command>make install</command></screen></para>
        </listitem>
        <listitem>
          <para>Start it as a daemon: 
      <screen><prompt>bash#</prompt> <command>whereis vm-pop3d</command>
<prompt>bash#</prompt> <command>/usr/local/sbin/vm-pop3d --help</command>
<prompt>bash#</prompt> <command>/usr/local/sbin/vm-pop3d --daemon=10</command></screen></para>
        </listitem>
        <listitem>
          <para>Add the last command above in <filename>/etc/rc.d/rc.local</filename> in order to start it automatically when the server is rebooted. </para>
        </listitem>
        <listitem>
          <para>Open the port 110 in the firewall. Add this line in <filename>/usr/local/config/firewall/inima-rules.sh</filename> : 
      <programlisting>./port.sh 110 accept INIMA  #pop</programlisting>
or this line in <filename>/usr/local/config/firewall/local-network-rules.sh</filename> : 
      <programlisting>./port.sh 110 accept LOCAL-NETWORK  #pop</programlisting></para>
        </listitem>
      </orderedlist>
      <para>The path of the virtual mailbox configurations is expected by <emphasis>vm-pop3d</emphasis> to be in <filename>/etc/virtual/domain.name/</filename> and the mailboxes are kept in <filename>/var/spool/virtual/domain.name/</filename> . These setting can be changed in the file <filename>vm-pop3d.h</filename> before compilation. However, I found it more suitable to add a link from <filename>/etc/virtual</filename> to <filename>/usr/local/exim/virtual/</filename> . </para>
      <para>Read also the README, INSTALL and the manual page of <emphasis>vm-pop3d</emphasis>. </para>
    </section>
    <section id="virt-mailboxes">
      <title>Mailboxes</title>
      <para>After the virtual maiboxes are created and <emphasis>vm-pop3d</emphasis> is installed and running, then the users will be able to access their mailboxes using a mail client (Eudora , Thunderbird, KMail, etc.) using the POP protocol. They should have a username and a password, which they have received from the administrator of the mailboxes. The username is something like this: <emphasis>user@domain.name</emphasis>, e.g. <emphasis>dasho@linux.inima.al</emphasis> (not just <emphasis>dasho</emphasis>). </para>
    </section>
  </section>
  <section id="installing">
    <title>Installing Exim as an Alternative to Sendmail</title>
    <orderedlist numeration="arabic" inheritnum="ignore" continuation="restarts">
      <listitem>
        <para>Create the log files: 
      <screen><prompt>bash#</prompt> <command>touch /var/log/exim_main.log</command>
<prompt>bash#</prompt> <command>touch /var/log/exim_panic.log</command>
<prompt>bash#</prompt> <command>touch /var/log/exim_reject.log</command>
<prompt>bash#</prompt> <command>chown mail.mail /var/log/exim_*</command></screen></para>
      </listitem>
      <listitem>
        <para>Tell <emphasis>tcpwrappers</emphasis> to allow the port <emphasis>exim</emphasis> for any host: 
      <screen><prompt>bash#</prompt> <command>vi /etc/hosts.allow</command></screen><programlisting>exim : ALL</programlisting></para>
      </listitem>
      <listitem>
        <para>Create the script <filename>/etc/init.d/exim</filename> ( <xref linkend="etc-initd-exim"/> ) and add exim in the list of services: 
      <screen><prompt>bash#</prompt> <command>vi /etc/init.d/exim</command>
<prompt>bash#</prompt> <command>chmod 755 /etc/init.d/exim</command>
<prompt>bash#</prompt> <command>/sbin/chkconfig --add exim</command>
<prompt>bash#</prompt> <command>/sbin/chkconfig --list exim</command></screen></para>
      </listitem>
      <listitem>
        <para>Create and run the script <filename>install-exim-alternative.sh</filename> ( <xref linkend="install-exim-alternative"/> ): 
      <screen><prompt>bash#</prompt> <command>mkdir /usr/local/exim/scripts</command>
<prompt>bash#</prompt> <command>cd /usr/local/exim/scripts</command>
<prompt>bash#</prompt> <command>vi install-exim-alternative.sh</command>
<prompt>bash#</prompt> <command>chmod 755 install-exim-alternative.sh</command>
<prompt>bash#</prompt> <command>./install-exim-alternative.sh</command>
<prompt>bash#</prompt> <command>/usr/sbin/alternatives --display mta</command>
<prompt>bash#</prompt> <command>/usr/sbin/alternatives --auto mta</command></screen></para>
      </listitem>
      <listitem>
        <para>Stop sendmail, set exim as MTA and start exim: 
      <screen><prompt>bash#</prompt> <command>/sbin/service sendmail stop</command>
<prompt>bash#</prompt> <command>/usr/sbin/alternatives --set mta /usr/local/exim/bin/exim</command>
<prompt>bash#</prompt> <command>/sbin/service exim start</command>
<prompt>bash#</prompt> <command>/sbin/chkconfig --level 2345 sendmail off</command>
<prompt>bash#</prompt> <command>/sbin/chkconfig --level 2345 exim on</command></screen></para>
      </listitem>
      <listitem>
        <para>In order to go back to sendmail (in case of problems), run these commands: 
      <screen><prompt>bash#</prompt> <command>/sbin/service exim stop</command>
<prompt>bash#</prompt> <command>/usr/sbin/alternatives --set mta /usr/sbin/sendmail.sendmail</command>
<prompt>bash#</prompt> <command>/sbin/service sendmail start</command>
<prompt>bash#</prompt> <command>/sbin/chkconfig --level 2345 exim off</command>
<prompt>bash#</prompt> <command>/sbin/chkconfig --level 2345 sendmail on</command></screen></para>
      </listitem>
      <listitem>
        <para>The switch between exim and sendmail is done more easily and more reliably using the script <filename>set-mta.sh</filename> : 
      <screen><prompt>bash#</prompt> <command>vi set-mta.sh</command>
<prompt>bash#</prompt> <command>chmod 755 set-mta.sh</command>
<prompt>bash#</prompt> <command>./set-mta.sh</command>
<prompt>bash#</prompt> <command>./set-mta.sh exim</command>
<prompt>bash#</prompt> <command>./set-mta.sh sendmail</command></screen></para>
      </listitem>
    </orderedlist>
    <section id="install-exim-sh">
      <title>scripts/install-exim.sh</title>
      <programlisting>
        <![CDATA[#!/bin/bash
#### a script that shows the steps for installing exim
#### (after compilation and configuration)

#### go to this directory
cd $(dirname $0)

#### add exim as a service alternative to sendmail
cp exim.init.d.sh /etc/init.d/exim
./install-exim-alternative.sh
/sbin/chkconfig --add exim
#/sbin/chkconfig --list exim

#### create exim log files
touch /var/log/exim_main.log
touch /var/log/exim_panic.log
touch /var/log/exim_reject.log
chown mail.mail /var/log/exim_*

#### create the spool directory
mkdir /var/spool/virtual
chown mail.mail /var/spool/virtual
chmod +t /var/spool/virtual

#### create a link to the config files of the virtual domains
ln -s /usr/local/exim/virtual /etc/virtual

#### stop sendmail and start exim
./set-mta.sh exim
]]>
      </programlisting>
    </section>
    <section id="etc-initd-exim">
      <title>/etc/init.d/exim</title>
      <programlisting>
        <![CDATA[#!/bin/bash
#
# exim    This shell script takes care of starting and stopping exim
#
# chkconfig: 2345 80 30
# description: Exim is a Mail Transport Agent, which is the program \
#              that moves mail from one machine to another.
# processname: exim
# config: /usr/local/exim/exim.conf
# pidfile: /var/run/exim.pid

# Source function library.
. /etc/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Source exim configuration.
if [ -f /etc/sysconfig/exim ] ; then
        . /etc/sysconfig/exim
else
        DAEMON=yes
        QUEUE=1h
fi

# Check that networking is up.
[ ${NETWORKING} = "no" ] && exit 0

EXIM=/usr/local/exim/bin/exim
[ -f $EXIM ] || exit 0

start() {
        # Start daemons.
        echo -n $"Starting $0: "
        daemon $EXIM $([ "$DAEMON" = yes ] && echo -bd) \
                              $([ -n "$QUEUE" ] && echo -q$QUEUE)
        RETVAL=$?
        echo
        [ $RETVAL = 0 ] && touch /var/lock/subsys/exim
}

stop() {
        # Stop daemons.
        echo -n $"Shutting down $0: "
        killproc exim
        RETVAL=$?
        echo
        [ $RETVAL = 0 ] && rm -f /var/lock/subsys/exim
}

restart() {
        stop
        start
}

# See how we were called.
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        restart
        ;;
  reload)
        restart
        ;;
  condrestart)
        [ -f /var/lock/subsys/exim ] && {
                stop
                [ -x /bin/chown ] && /bin/chown mail.mail -R /var/spool/exim
                start
        } || {
                [ -x /bin/chown ] && /bin/chown mail.mail -R /var/spool/exim
        }
        ;;
  status)
        status exim
        ;;
  *)
        echo $"Usage: $0 {start|stop|restart|reload|status|condrestart}"
        exit 1
esac

exit $RETVAL
]]>
      </programlisting>
    </section>
    <section id="install-exim-alternative">
      <title>scripts/install-exim-alternative.sh</title>
      <programlisting>
        <![CDATA[#!/bin/bash
/usr/sbin/alternatives --install /usr/sbin/sendmail mta \
                                 /usr/local/exim/bin/exim 90 \
  --slave /usr/bin/mailq mta-mailq /usr/local/exim/bin/exim \
  --slave /usr/bin/newaliases mta-newaliases /usr/bin/newaliases.sendmail \
  --slave /usr/bin/rmail mta-rmail /usr/local/exim/bin/exim \
  --slave /usr/share/man/man1/mailq.1.gz mta-mailqman \
          /usr/share/man/man1/mailq.sendmail.1.gz \
  --slave /usr/share/man/man1/newaliases.1.gz mta-newaliasesman \
          /usr/share/man/man1/newaliases.sendmail.1.gz \
  --slave /usr/share/man/man5/aliases.5.gz mta-aliasesman \
          /usr/share/man/man5/aliases.sendmail.5.gz \
  --initscript exim
]]>
      </programlisting>
    </section>
    <section id="set-mta-sh">
      <title>scripts/set-mta.sh</title>
      <programlisting>
        <![CDATA[#!/bin/bash
#### set MTA (Mail Transport Agent) to sendmail or exim

case "${1}" in
  ls )
    /usr/sbin/alternatives --display mta
    exit 0
    ;;
  exim )
    /sbin/service sendmail stop
    /usr/sbin/alternatives --set mta /usr/local/exim/bin/exim
    /sbin/service exim start
    /sbin/chkconfig sendmail off
    /sbin/chkconfig --level 2345 exim on
    exit 0
    ;;
  sendmail )
    /sbin/service exim stop
    /usr/sbin/alternatives --set mta /usr/sbin/sendmail.sendmail
    /sbin/service sendmail start
    /sbin/chkconfig exim off
    /sbin/chkconfig --level 2345 sendmail on
    exit 0
    ;;
  * )
     echo "Usage: ${0} [ ls | exim | sendmail ]"
     exit 0
     ;;
esac
]]>
      </programlisting>
    </section>
  </section>
  <section id="testing">
    <title>Testing Exim</title>
    <itemizedlist>
      <listitem>
        <para>Checking that the config file is OK:
<screen>
<prompt>bash$</prompt> <command>/usr/local/exim/bin/exim -bV</command>
<prompt>bash$</prompt> <command>/usr/local/exim/bin/exim -bV -C /usr/local/exim/exim_1.conf</command>
</screen></para>
      </listitem>
      <listitem>
        <para>Checking some addresses:
<screen>
<prompt>bash$</prompt> <command>/usr/local/exim/bin/exim -bt dasho@dns3.inima.al</command>
<prompt>bash$</prompt> <command>/usr/local/exim/bin/exim -bt dhoxha@inima.al</command>
<prompt>bash$</prompt> <command>/usr/local/exim/bin/exim -bt dashohoxha@yahoo.com</command>
</screen></para>
      </listitem>
      <listitem>
        <para>Check sending an e-mail:
<screen>
<prompt>bash#</prompt> <command>/usr/local/exim/bin/exim -v dhoxha@inima.al</command>
From: dasho@dns3.inima.al
To: dhoxha@inima.al
Subject: Testing exim

Testing exim
.
</screen></para>
      </listitem>
      <listitem>
        <para>How to test with full debug output using a specific config file:
<screen>
<prompt>bash#</prompt> <command>/usr/local/exim/bin/exim -C /usr/local/exim/exim_1.conf -d -bt dhoxha@inima.al</command>
</screen></para>
      </listitem>
      <listitem>
        <para>How to test as if coming from a specified ip address:
<screen>
<prompt>bash#</prompt> <command>/usr/local/exim/bin/exim -bh 192.168.10.2</command>
HELO inima.al
MAIL FROM: dasho@dns3.inima.al
RCPT TO: dhoxha@inima.al
DATA
Subject: something
your message here
.
</screen></para>
      </listitem>
    </itemizedlist>
  </section>
  <section id="webmail">
    <title>Accessing Mailboxes From Web</title>
    <para>To access mailboxes from the web, a webmail program is installed. We have used a modified version of <ulink url="http://sourceforge.net/projects/uebimiau/">UebiMiau</ulink> . Configuration of the application is done in <filename>inc/config.php</filename> , and configuration of the laguages is done in <filename>inc/config.languages.php</filename> . </para>
    <para>The modifications that are made to this application allow each user to modify his own own password in the <emphasis>Preferences</emphasis> section. They also allow for different virtual domains to have their own logos. </para>
    <para>However the most importent improvment is that the user admin@domain.name is able to manage the mailboxes from the web. He can can create new mailboxes, reset the password of existing mailboxes, delete mailboxes, create/delete forwarders (aliases), etc. </para>
  </section>
  <section id="riinstalling">
    <title>Installing in Another Server</title>
    <para>If virtual email domains are already installed in a mailserver (with exim, webmail and vm-pop3d), then it can be installed easily in another similar server. </para>
    <para>In order to do this, follow these steps: </para>
    <orderedlist numeration="arabic" inheritnum="ignore" continuation="restarts">
      <listitem>
        <para>Make a copy of <filename>/usr/local/exim/</filename> and <filename>/var/www/html/tools/webmail/</filename> to the other server (tar, transfer, untar): 
      <screen><prompt>bash#</prompt> <command>cd /usr/local/</command>
<prompt>bash#</prompt> <command>tar cfz exim.tgz exim/</command>
<prompt>bash#</prompt> <command>scp exim.tgz 192.168.251.195:/usr/local/</command>
<prompt>bash#</prompt> <command>cd /var/www/html/tools/</command>
<prompt>bash#</prompt> <command>tar cfz webmail.tgz webmail/</command>
<prompt>bash#</prompt> <command>scp webmail.tgz 192.168.251.195:/var/www/html/tools/</command>
<prompt>bash#</prompt> <command>ssh 192.168.251.195</command>
<prompt>bash#</prompt> <command>cd /usr/local/</command>
<prompt>bash#</prompt> <command>tar xfz exim.tgz</command>
<prompt>bash#</prompt> <command>cd /var/www/html/tools/</command>
<prompt>bash#</prompt> <command>tar xfz webmail.tgz</command></screen></para>
      </listitem>
      <listitem>
        <para>Modify <filename>/usr/local/exim/etc/exim.conf</filename> by changing the hostname, etc. </para>
      </listitem>
      <listitem>
        <para>Modify the virtual domains by using the scripts <filename>/usr/local/exim/scripts/del-domain.sh</filename> and <filename>/usr/local/exim/scripts/add-domain.sh</filename> . </para>
      </listitem>
      <listitem>
        <para>Modify the config of webmail: <filename>inc/config.php</filename> , <filename>inc/config.languages.php</filename> , <filename>domains/</filename> , etc. </para>
      </listitem>
      <listitem>
        <para>Install <emphasis>vm-pop3d</emphasis>. </para>
      </listitem>
      <listitem>
        <para>Finally, install and run exim: 
      <screen><prompt>bash#</prompt> <command>cd /usr/local/exim/scripts/</command>
<prompt>bash#</prompt> <command>./install-exim.sh</command></screen></para>
      </listitem>
    </orderedlist>
  </section>
  <section id="references">
    <title>References and Related Links</title>
    <para>Related links: </para>
    <itemizedlist>
      <listitem>
        <para>
          <ulink url="http://www.flatmtn.com/computer/Linux-Exim4.html"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://192.168.251.195/docs/exim-html-4.40/doc/html/"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://192.168.251.195/docs/exim-pdf-4.40/doc/"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://www.exim.org/exim-html-4.40/doc/html/spec_toc.html"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://www.exim.org/exim-html-4.40/doc/html/filter_toc.html"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://www.hants.lug.org.uk/cgi-bin/wiki.pl?LinuxHints/EximVirtualDomains"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://www.tty1.net/virtual_domains_en.html"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://koivi.com/exim4-config/"/>
        </para>
      </listitem>
      <listitem>
        <para>
          <ulink url="http://silverwraith.com/vexim/index.html"/>
        </para>
      </listitem>
    </itemizedlist>
  </section>
</article>
