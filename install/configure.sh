#!/bin/bash

### go to the root dir
cd $(dirname $0)/..

### modify the php scripts in 'content/' by changing
### the first line to the correct path of the php
php=$(which php)
find content/ -name '*.php' | xargs sed "1 c #\!$php -q" -i

### modify search/*.sh by changing $swishe; modify books.cfg as well
swishe=$(which swish-e)
sed -i "/swishe=/ c swishe=$swishe" search/*.sh
sed -i "/SWISH_E=/ c SWISH_E='$swishe'" docbookwiki.conf

### modify content/downloads/dblatex.sh by changing $dblatexpath
dblatex=$(which dblatex)
dblatex_file=content/downloads/dblatex.sh
sed -i "/dblatexpath=/ c dblatexpath=$dblatex" $dblatex_file
