#!/bin/bash
### make the binary formats of the translation files

### go to this dir
cd $(dirname $0)

langs="en sq_AL de it nl fr"
for lng in $langs
do
  ./msgfmt.sh $lng
done

