#!/bin/bash
### The directory 'content/' should be moved to another
### filesystem (partition) with ReiserFS format, which
### has better performance than ext3 for many small files.

### go to this directory
cd $(dirname $0)

### mount the content filesystem
mount --bind /mnt/hda9/books-quran/content/ content/

