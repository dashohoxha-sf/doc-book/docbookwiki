#!/bin/bash
### index all the books

### go to this dir
cd $(dirname $0)

dir=../content/books/xml/
swishe=/usr/local/bin/swish-e

$swishe -i $dir -c global_index.cfg -f global.index
$swishe -i $dir -c tag_index.cfg -f tag.index

#######################################################################
# Two index files are created according to two config files.
# During the search, both the global index and the tag index are
# searched.  This is done in order to allow both global searching and
# tag searching.  According to the docs of swish-e it should work
# without the need for this trick, however it seems not to work.
# (
#   E.g. if I use the directive: MetaNames command
#   then the search for 'command=ls' will give results, but the
#   search for 'ls' will not give any results. With 2 indexes,
#   both of them will give results, however the search for
#   'command=ls and ls' will yield no results.
# )
#######################################################################


