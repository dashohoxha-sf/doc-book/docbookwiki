// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  DocBookWiki.  DocBookWiki is a web application
  that displays and edits DocBook documents.

  Copyright (C) 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  DocBookWiki is free software;  you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  DocBookWiki is distributed  in the hope that it  will be useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along  with  DocBookWiki;  if   not,  write  to  the  Free  Software
  Foundation, Inc., 59 Temple  Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function show_book_list()
{
  SendEvent('book_list', 'set_visible', 'visible=true');
}

function hide_book_list()
{
  SendEvent('book_list', 'set_visible', 'visible=false');
}

function del_book()
{
  var form = document.delete_book;
  var book_id = form.del_bookid.value;
  var lng = form.del_lng.value;
  var msg;

  if (book_id=='all')
    {
      msg = T_("You are deleting all the books!");
      if (!confirm(msg)) return;
    }
  else if (lng=='')
    {
      msg = T_("You are deleting the book 'v_book_id' (all languages)!");
      msg = msg.replace(/v_book_id/, book_id);
      if (!confirm(msg)) return;
    }
  else
    {
      msg = T_("You are deleting the book 'v_book_id (v_lng)'!");
      msg = msg.replace(/v_book_id/, book_id);
      msg = msg.replace(/v_lng/, lng);
      if (!confirm(msg)) return;
    }

  var event_args = 'book_id=' + book_id + ';lng=' + lng;
  SendEvent('book_list', 'delete', event_args);
}
