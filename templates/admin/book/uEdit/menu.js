// -*-C-*- //tell emacs to use the C mode

//modify the structure of the menu by adding a root to it
MENU_ITEMS.unshift('Books', null, null);
MENU_ITEMS = [ MENU_ITEMS ];

//modify some properties of the menu
MENU_POS[0].width = 50;
MENU_POS[0].block_top = 52;
MENU_POS[0].block_left = 165;
MENU_POS[1].block_left = 45;

//change the action that happens when a menu item is clicked
function book(book_id)
{
  //alert(book_id);
  document.uEdit.books.value += book_id + "\n";
}
