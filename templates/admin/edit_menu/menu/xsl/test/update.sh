#!/bin/bash

id=nontechnical
bookid=non-tech
caption='Non Technical'
xsl_file=../update.xsl
xml_file=../../menu.xml

xsltproc  --stringparam id "$id" \
          --stringparam bookid "$bookid" \
          --stringparam caption "$caption" \
          $xsl_file  $xml_file
