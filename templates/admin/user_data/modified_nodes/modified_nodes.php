<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once SCRIPTS.'user_data.php';

/**
 * Displays a list of the modified nodes that can be approved by the user.
 *
 * @package admin
 * @subpackage user_data
 */
class modified_nodes extends WebObject
{
  /**
   * First find the books where the user has some access rights.
   * Then find the modified nodes in each of these books.
   * Next, check whether the user has approve rights on these
   * modified nodes, and if yes, add them to the recordset 'modified_nodes'.
   */
  function onRender()
  {
    $rs = new EditableRS('modified_nodes');

    //get the books for which the user is admin
    $arr_books_admin = $this->get_arr_books_admin();

    //add in the recordset all the modified nodes in the books
    //for which the user is admin
    $this->add_is_admin($rs, $arr_books_admin);

    //find the books where the user has some access rights and is not admin
    $arr_books_accr = $this->get_arr_books_accr($arr_books_admin);

    //find the modified nodes in these books that can be approved 
    //by user and add them in the recordset
    $this->add_has_accr($rs, $arr_books_accr);

    //add the recordset to the webPage
    global $webPage;
    $webPage->addRecordset($rs);
  }

  /** return an array of the books for which the user is admin */
  function get_arr_books_admin()
  {
    $user_data = get_user_data(USER);
    $books = trim($user_data['books']);
    $arr_books = explode(',', $books);

    $last_idx = sizeof($arr_books) - 1;
    if ($arr_books[$last_idx]=='')  unset($arr_books[$last_idx]);

    return $arr_books;
  }

  /**
   * Add in the recordset all the modified nodes in the books
   * for which the user is admin.
   */
  function add_is_admin(&$rs, $arr_books_admin)
  {
    for ($i=0; $i < sizeof($arr_books_admin); $i++)
      {
        $book_id = $arr_books_admin[$i];
        $book_path = WS_BOOKS.$book_id.'/';
        $output = shell("ls $book_path/");
        $arr_langs = explode("\n", trim($output));
        for ($l=0; $l < sizeof($arr_langs); $l++)
          {
            $lng = $arr_langs[$l];
            $filename = "$book_path/$lng/modified_nodes.txt";
            if (!file_exists($filename))  continue;
            $arr_modified_nodes = file($filename);
            for ($n=0; $n < sizeof($arr_modified_nodes); $n++)
              {
                $node_path = $arr_modified_nodes[$n];
                $node_path = trim($node_path);
                $node_title = $this->get_title($book_id, $lng, $node_path);
                $rec = compact('book_id', 'lng', 'node_path', 'node_title');
                $rs->addRec($rec);          
              }
          }
      }
  }

  /**
   * Return an array with the books where the user has 
   * some access rights and is not admin.
   */
  function get_arr_books_accr($arr_books_admin)
  {
    $arr_books_accr = array();

    $accr_path = ADMIN.'access_rights/';
    $user = USER;
    $output = shell("find $accr_path -type f -name '$user'");
    $arr_files = explode("\n", $output);

    for ($i=0; $i < sizeof($arr_files); $i++)
      {
        $file = trim($arr_files[$i]);
        if ($file=='')  continue;
        $book_id = basename(dirname($file));
        if (!in_array($book_id, $arr_books_admin))
          {
            $arr_books_accr[] = $book_id;
          }
      }

    return $arr_books_accr;
  }

  /**
   * Find the modified nodes that can be approved 
   * by user and add them in the recordset.
   */
  function add_has_accr(&$rs, $arr_books_accr)
  {
    for ($i=0; $i < sizeof($arr_books_accr); $i++)
      {
        $book_id = $arr_books_accr[$i];

        //get the access right rules for this book and the current user
        $accr_file = ADMIN . "access_rights/$book_id/" . USER;
        $accr_rules = file($accr_file);

        //get the languages of the book
        $book_path = WS_BOOKS.$book_id.'/';
        $output = shell("ls $book_path/");
        $arr_langs = explode("\n", trim($output));

        //process the modified nodes for all the languages of the book
        for ($l=0; $l < sizeof($arr_langs); $l++)
          {
            $lng = $arr_langs[$l];
            $filename = "$book_path/$lng/modified_nodes.txt";
            if (!file_exists($filename))  continue;
            $arr_modified_nodes = file($filename);
            for ($n=0; $n < sizeof($arr_modified_nodes); $n++)
              {
                $node_path = $arr_modified_nodes[$n];
                $node_path = trim($node_path);

                //add the modified node to recordset,
                //if the user can approve it
                if ($this->has_approve_right($lng, $node_path, $accr_rules))
                  {
                    $node_title = $this->get_title($book_id,$lng,$node_path);
                    $rec = compact('book_id','lng','node_path','node_title');
                    $rs->addRec($rec);
                  }
              }
          }
      }
  }

  /** Returns true if the user has approve right on the given node. */
  function has_approve_right($lng, $node_path, $arr_access_rights)
  {
    //try to match the given node and language with the access rights
    $approve = false;
    for ($i=0; $i < sizeof($arr_access_rights); $i++)
      {
        $line = trim($arr_access_rights[$i]);
        list($access,$levels,$nodes,$langs) = explode(':', $line);

        //if this line matches the given node and language 
        //then set the value of approve according to it
        if ($this->node_match($node_path, $nodes)
            and $this->lang_match($lng, $langs))
          {
            $arr_levels = explode(',', $levels);
            if (in_array('approve', $arr_levels))
              {
                $approve = ($access=='allow' ? true : false);
              }
          }
      }

    return $approve;
  }

  /**
   * Returns true if one of the node_path expressions in the list
   * matches the given node path, otherwise returns false.
   * The node_list is a comma separated list of node_path expressions
   * (which are actually regular expressions), or is the string 'ALL'.
   * 'ALL' matches any node path.
   */
  function node_match($node_path, $node_list)
  {
    if (strtoupper($node_list)=='ALL')  return true;

    $arr_nodes = explode(',', $node_list);
    for ($i=0; $i < sizeof($arr_nodes); $i++)
      {
        $expr = $arr_nodes[$i];
        if (ereg('^'.$expr, $node_path)) return true;
      }
  }

  /**
   * Returns true if one of the language ids in the list matches
   * the given language, otherwise returns false. The lang_list
   * is a comma separated list of languages, or 'ALL'.
   * 'ALL' matches any language.
   */
  function lang_match($lng, $lang_list)
  {
    if (strtoupper($lang_list)=='ALL')  return true;

    $arr_langs = explode(',', $lang_list);
    $match = in_array($lng, $arr_langs);

    return $match;
  }

  function get_title($book_id, $lng, $node_path)
  {
    $cache_path = WS_CACHE."$book_id/$lng/";
    $navigation_file = $cache_path.$node_path."navigation.txt";
    $line = shell("grep full_title $navigation_file");
    $arr = split('=', chop($line), 2);
    $title = $arr[1];
    if ($title=='')  $title = 'Table Of Contents';
    return $title;
  }
}
?>