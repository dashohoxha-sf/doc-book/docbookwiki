<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  //we need the function xml_to_text()
include_once DOCBOOK.'/edit/content/process_content.php';

//we need the function update_cache_files()
include_once DOCBOOK.'/edit/edit_funcs.php';

/**
 * Review and approve (or cancel) the modifications that are done
 * to the current node of the book.
 *
 * @package docbook
 * @subpackage approve
 */
class approve extends WebObject
{
  function on_set_lock($event_args)
  {
    $lock = $event_args['lock'];
    set_node_lock($lock, 'approve');
  }

  function on_revert($event_args)
  {
    $recursive = ($event_args['recursive'] == 'true');
    if ($recursive)
      $this->recursive_revert();
    else
      $this->non_recursive_revert();
  }

  function non_recursive_revert()
  {
    $workspace_xml_file = file_content_xml(WS_BOOKS);
    $node_dir = dirname($workspace_xml_file);

    //revert
    shell(SCRIPTS."approve/svn_revert.sh $node_dir");

    //update the cache file content.html in the workspace
    update_cache();

    //update subnodes.html and navigation.txt as well
    $node_path = WebApp::getSVar('docbook->node_path');
    update_cache_files($node_path);

    //set the status of the node to synchronized
    set_node_status('synchronized');

    //remove this node from the list of the modified nodes
    remove_from_modified_nodes();
  }

  function recursive_revert()
  {
    $workspace_xml_file = file_content_xml(WS_BOOKS);
    $node_dir = dirname($workspace_xml_file);
    $node_dir = str_replace('./', '', $node_dir);

    //revert all the content.xml files 
    $result = shell("svn revert -R $node_dir");
    $result = str_replace($node_dir.'/', '', $result);
    if ($result != '')  WebApp::message($result);

    //update cache files in workspace
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $node_path = WebApp::getSVar('docbook->node_path');
    shell(CONTENT."cache/cache.sh $book_id $lng 'workspace' '$node_path'");

    //set the status of this node and all the subnodes to synchronized
    $this->set_all_synchronized($node_dir);

    //remove this node and the subnodes from the list of the modified nodes
    remove_from_modified_nodes($node_path, true);
  }

  function set_all_synchronized($node_dir)
  {
    shell(SCRIPTS."approve/set_synchronized.sh $node_dir");
  }

  function on_commit($event_args)
  {
    $log = trim($event_args['log']);

    $recursive = ($event_args['recursive'] == 'true');
    if ($recursive) 
      $this->recursive_commit($log);
    else
      $this->non_recursive_commit($log);
  }

  function non_recursive_commit($log)
  {
    //make up the commit message
    if ($log!='')  $log = "\nLog: ".$log;
    $arr_state = get_node_state();
    extract($arr_state);
    $m_time = date('Y-m-d H:i', $m_timestamp);
    $time = date('Y-m-d H:i');
    $msg = ("Modified by $m_user, $m_time. "
            . "Approved by ".USER.", $time".$log);

    //commit any modifications
    $workspace_xml_file = file_content_xml(WS_BOOKS);
    $node_dir = dirname($workspace_xml_file);
    shell(SCRIPTS."approve/svn_commit.sh $node_dir '$msg'");

    //check whether the book is fixed to a tag
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $tag = book_fixed_to_tag($book_id, $lng);

    if ($tag)
      $this->msg_book_fixed_to_tag($tag);
    else
      {
        //update the content.xml and media files in the public copy
        $public_xml_file = file_content_xml(BOOKS);
        $node_dir = dirname($public_xml_file);
        shell("svn update -N $node_dir");

        //update the cache file content.html of the public copy
        update_cache('public');
        //update subnodes.html and navigation.txt as well
        $node_path = WebApp::getSVar('docbook->node_path');
        update_cache_files($node_path);
      }

    //set the status of the node to synchronized
    set_node_status('synchronized');

    //remove this node from the list of the modified nodes
    remove_from_modified_nodes();
  }

  function msg_book_fixed_to_tag($tag)
  {
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');

    $msg = T_("The book (v_book_id, v_lng) is fixed to v_tag, \
so the changes will not be displayed in the public copy.");

    $msg = str_replace('v_book_id', $book_id, $msg);
    $msg = str_replace('v_lng', $lng, $msg);
    $msg = str_replace('v_tag', $tag, $msg);

    WebApp::message($msg);
  }

  function recursive_commit($log)
  {
    //commit all the content.xml files 
    if ($log!='')  $log = "\nLog: ".$log;
    $workspace_xml_file = file_content_xml(WS_BOOKS);
    $node_dir = dirname($workspace_xml_file);
    $msg = date('Y-m-d H:i').' >> Approved by '.USER . $log;
    shell("svn commit $node_dir -m '$msg'");

    //check whether the book is fixed to a tag
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $tag = book_fixed_to_tag($book_id, $lng);

    if ($tag)
      $this->msg_book_fixed_to_tag($tag);
    else
      {
        //update the content.xml and media files in the public copy
        $public_xml_file = file_content_xml(BOOKS);
        $node_dir = dirname($public_xml_file).'/';
        shell("svn update $node_dir");

        //update cache files
        $node_path = WebApp::getSVar('docbook->node_path');
        shell(CONTENT."cache/cache.sh $book_id $lng 'books' '$node_path'");
        //update subnodes.html as well
        update_cache_files($node_path);
      }

    //set the status of this node and all the subnodes to synchronized
    $node_dir = dirname($workspace_xml_file);
    $this->set_all_synchronized($node_dir);

    //remove this node and the subnodes from the list of the modified nodes
    $node_path = WebApp::getSVar('docbook->node_path');
    remove_from_modified_nodes($node_path, true);
  }

  function onRender()
  {
    //add variables {{status}}, {{m_user}}, {{m_email}}, {{m_time}}
    $arr_state = get_node_state();
    WebApp::addVar('status', $arr_state['status']);
    $m_user = $arr_state['m_user'];
    $m_email = $arr_state['m_email'];
    WebApp::addVar('m_user', "<a href='mailto:$m_email'>$m_user</a>");
    $m_time = get_date_str($arr_state['m_timestamp']);
    WebApp::addVar('m_time', $m_time);

    //add the variables {{locked_by_somebody}} and {{locked}}
    $locked_by_somebody = locked_by_somebody($arr_state);
    $str_locked_by_somebody = ($locked_by_somebody ? 'true' : 'false');
    $str_locked = (is_locked($arr_state) ? 'true' : 'false');
    WebApp::addVar('locked_by_somebody', $str_locked_by_somebody);
    WebApp::addVar('locked', $str_locked);

    //display a notification message if the node is locked
    if ($locked_by_somebody)
      {
        extract($arr_state);
        $msg = T_("This node is locked for v_mode\n\
by v_l_user (v_l_email).\n\
Please try again later.");
        $msg = str_replace('v_mode', $mode, $msg);
        $msg = str_replace('v_l_user', $l_user, $msg);
        $msg = str_replace('v_l_email', $l_email, $msg);
        WebApp::message($msg);
        WebApp::addVar('l_mode', $mode);
        WebApp::addVar('l_user', "<a href='mailto:$l_email'>$l_user</a>");
        return;
      }

    //add {{id}}
    ereg('([^/]+)/$', $node_path, $regs);
    $id = $regs[1];
    if ($id=='.')  $id = WebApp::getSVar('docbook->book_id');
    WebApp::addVar("id", $id);

    //add the {{content_diff}} variable
    $content_diff = $this->get_content_diff();
    WebApp::addVar('content_diff', $content_diff."\n");

    //add the {{svn_status}} variable
    $svn_status = $this->get_svn_status();
    WebApp::addVar('svn_status', $svn_status."\n");

    $view_revisions = (trim($diff)=='' ? 'true' : 'false');
    WebApp::addVar('view_revisions', $view_revisions);
  }

  /** get the modifications of content.xml */
  function get_content_diff()
  {
    $file_xml = file_content_xml();
    $get_title_xsl = XSLT.'edit/get_content_title.xsl';

    //get the HEAD revision of the file
    $xml_HEAD = shell("svn cat $file_xml -r HEAD");
    $file_xml_head = write_tmp_file($xml_HEAD);

    //convert to TextWiki the HEAD revision
    $title = shell("xsltproc $get_title_xsl $file_xml_head");
    $txt = get_node_content($file_xml_head, 'text');
    $txt = 'T: '.$title."\n\n".trim($txt)."\n";
    $file_txt_head = write_tmp_file($txt);

    //convert to TextWiki the working copy
    $title = shell("xsltproc $get_title_xsl $file_xml");
    $txt = get_node_content($file_xml, 'text');
    $txt = 'T: '.$title."\n\n".trim($txt)."\n";
    $file_txt = write_tmp_file($txt);

    //get the difference
    $diff = shell("diff -ubB $file_txt_head $file_txt | sed '1,2d'");

    //clean the temporary files
    unlink($file_xml_head);
    unlink($file_txt_head);
    unlink($file_txt);

    //format it for propper display in html
    $diff = htmlspecialchars($diff);
    $patterns = array(
                      '/^(@@.*@@)$/m',
                      '/^-(.*)$/m',
                      '/^\+(.*)$/m',
                      '/^ (.*)$/m'
                      );
    $replacements = array(
                          '<div class="line_nr">\\1</div>',
                          '<div class="removed">&nbsp;\\1</div>',
                          '<div class="added">&nbsp;\\1</div>',
                          '<div class="context">&nbsp;\\1</div>'
                          );
    $diff = preg_replace($patterns, $replacements, $diff);

    return $diff;
  }

  /**
   * Get the modifications made in media files 
   * (added/replaced/deleted images etc.)
   */
  function get_svn_status()
  {
    //get the path of the section
    $path = dirname(file_content_xml());
    $path = str_replace('./', '', $path);
    $path .= '/';

    //get the difference
    $sed_expr = "-e '/^\\? /d' -e '/state\\.txt/d' -e 's#$path##'";
    $status = shell("svn status $path | sed $sed_expr");

    //format it for propper display in html
    $status = trim($status);
    $patterns = array(
                      '/^(M +.*)$/m',
                      '/^(D +.*)$/m',
                      '/^(A +.*)$/m'
                      );
    $replacements = array(
                          '<span class="modified">\\1</span>',
                          '<span class="removed">\\1</span>',
                          '<span class="added">\\1</span>'
                          );
    $status = preg_replace($patterns, $replacements, $status);

    return $status;
  }
}
?>