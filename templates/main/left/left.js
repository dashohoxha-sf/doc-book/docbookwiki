// -*-C-*-
/*
  This file is part of  DocBookWiki.  DocBookWiki is a web application
  that displays and edits DocBook documents.

  Copyright (C) 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  DocBookWiki is free software;  you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  DocBookWiki is distributed  in the hope that it  will be useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along  with  DocBookWiki;  if   not,  write  to  the  Free  Software
  Foundation, Inc., 59 Temple  Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function edit_menu1(visible)
{
  SendEvent('main','edit_menu');
}

function search1()
{
  var form = document.form_search;
  var words = form.search_words.value;
  SendEvent('main', 'search', 'words='+words);
}

function get_tar_gz(format)
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/tar_gz/' + book_id + '/' + lng + '/'
    + book_id + '.' + lng + '.' + format + '.tar.gz';
  window.location = url;
}

function get_xml()
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/formats/' + book_id + '/' + lng + '/xml/'
    + book_id + '.' + lng + '.xml';
  window.location = url;
}

function get_tex()
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/formats/' + book_id + '/' + lng + '/tex/'
    + book_id + '.' + lng + '.dblatex.tex';
  window.location = url;
}

function get_pdf()
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/formats/' + book_id + '/' + lng + '/pdf/'
    + book_id + '.' + lng + '.dblatex.pdf';
  window.location = url;
}

function get_html1()
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/formats/' + book_id + '/' + lng + '/html1/' 
    + book_id + '.' + lng + '.xslt/' + book_id + '.' + lng + '.xslt.html';
  var html1 = window.open(url);
}

function get_txt()
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/formats/' + book_id + '/' + lng 
    + '/txt/' + book_id + '.' + lng + '.xmlto.txt';
  var html1 = window.open(url);
}
