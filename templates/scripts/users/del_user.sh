#!/bin/bash
### delete the given user from the list of users
### and remove any access rights of him

### get the username from the first parameter
username="$1"

### the directory where the access rights are kept
access_rights=templates/admin/access_rights

### check that the username is not 'users'
### (otherwise it will delete the file wich keeps the list of users)
if [ "$username" = "users" ]; then exit; fi

### delete the record of the given user from the list of users
sed -i -n "/^$username:/!p" $access_rights/users

### remove any access rights of the user in the books
find $access_rights/ -type f -name $username | xargs rm
