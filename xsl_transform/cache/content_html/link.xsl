<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>


<!-- ulink -->
<xsl:template match="ulink">
  <xsl:variable name="content">
    <xsl:value-of select="normalize-space(.)" />
  </xsl:variable>

  <xsl:variable name="label">
    <xsl:choose>
      <xsl:when test="$content!=''"><xsl:value-of select="$content"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="@url"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <a class="ulink" href="{@url}" target="_blank">
    <xsl:value-of select="$label"/>
  </a>
</xsl:template>


<!-- xref -->
<xsl:template match="xref">
<a class="xref" href="javascript:set_node_id('{@linkend}')">
  <xsl:value-of select="@linkend" />
</a>
</xsl:template>


</xsl:transform>
