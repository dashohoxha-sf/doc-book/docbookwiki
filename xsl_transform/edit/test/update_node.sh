#!/bin/bash

xml_file=../../../content/books/xml/linux_server_admin/en/index.xml
node_path=./basicservices/network/
new_id=test

xsltproc --stringparam path "$node_path" \
         --stringparam new_id "$new_id" \
         ../update_node.xsl $xml_file

