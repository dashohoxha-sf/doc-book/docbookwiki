<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Converts xml tag names to html tag names -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!-- for an xml tag or attribute, get the corresponding short name -->
<xsl:template name="get-short-name">
  <xsl:param name="name" />

  <xsl:choose>

    <!-- preformated elements -->

    <xsl:when test="$name='screen'">
      <xsl:value-of select="'scr'"/>
    </xsl:when>

    <xsl:when test="$name='literallayout'">
      <xsl:value-of select="'ll'"/>
    </xsl:when>

    <xsl:when test="$name='programlisting'">
      <xsl:value-of select="'code'"/>
    </xsl:when>

    <!-- list elements -->

    <xsl:when test="$name='itemizedlist'">
      <xsl:value-of select="'ul'"/>
    </xsl:when>

    <xsl:when test="$name='orderedlist'">
      <xsl:value-of select="'ol'"/>
    </xsl:when>

    <xsl:when test="$name='listitem'">
      <xsl:value-of select="'li'"/>
    </xsl:when>

    <!-- admonitions -->

    <xsl:when test="$name='note'">
      <xsl:value-of select="'n'"/>
    </xsl:when>

    <xsl:when test="$name='tip'">
      <xsl:value-of select="'tip'"/>
    </xsl:when>

    <xsl:when test="$name='caution'">
      <xsl:value-of select="'caution'"/>
    </xsl:when>

    <xsl:when test="$name='important'">
      <xsl:value-of select="'imp'"/>
    </xsl:when>

    <xsl:when test="$name='warning'">
      <xsl:value-of select="'w'"/>
    </xsl:when>

    <!-- block elements -->

    <xsl:when test="$name='para'">
      <xsl:value-of select="'p'"/>
    </xsl:when>

    <xsl:when test="$name='title'">
      <xsl:value-of select="'t'"/>
    </xsl:when>

    <xsl:when test="$name='figure'">
      <xsl:value-of select="'fig'"/>
    </xsl:when>

    <xsl:when test="$name='example'">
      <xsl:value-of select="'xmp'"/>
    </xsl:when>

    <xsl:when test="$name='footnote'">
      <xsl:value-of select="'fn'"/>
    </xsl:when>

    <!-- inline elements -->

    <xsl:when test="$name='ulink'">
      <xsl:value-of select="'a'"/>
    </xsl:when>

    <xsl:when test="$name='prompt'">
      <xsl:value-of select="'pr'"/>
    </xsl:when>

    <xsl:when test="$name='command'">
      <xsl:value-of select="'c'"/>
    </xsl:when>

    <xsl:when test="$name='option'">
      <xsl:value-of select="'o'"/>
    </xsl:when>

    <xsl:when test="$name='application'">
      <xsl:value-of select="'app'"/>
    </xsl:when>

    <xsl:when test="$name='filename'">
      <xsl:value-of select="'f'"/>
    </xsl:when>

    <xsl:when test="$name='computeroutput'">
      <xsl:value-of select="'co'"/>
    </xsl:when>

    <xsl:when test="$name='userinput'">
      <xsl:value-of select="'ui'"/>
    </xsl:when>

    <xsl:when test="$name='replaceable'">
      <xsl:value-of select="'r'"/>
    </xsl:when>

    <xsl:when test="$name='trademark'">
      <xsl:value-of select="'tm'"/>
    </xsl:when>

    <xsl:when test="$name='firstterm'">
      <xsl:value-of select="'ft'"/>
    </xsl:when>

    <xsl:when test="$name='acronym'">
      <xsl:value-of select="'acr'"/>
    </xsl:when>

    <xsl:when test="$name='wordasword'">
      <xsl:value-of select="'waw'"/>
    </xsl:when>

    <xsl:when test="$name='emphasis'">
      <xsl:value-of select="'em'"/>
    </xsl:when>

    <!-- gui... -->

    <xsl:when test="$name='guilabel'">
      <xsl:value-of select="'gl'"/>
    </xsl:when>

    <xsl:when test="$name='guibutton'">
      <xsl:value-of select="'gb'"/>
    </xsl:when>

    <xsl:when test="$name='guiicon'">
      <xsl:value-of select="'gi'"/>
    </xsl:when>

    <xsl:when test="$name='guimenu'">
      <xsl:value-of select="'gm'"/>
    </xsl:when>

    <xsl:when test="$name='guimenuitem'">
      <xsl:value-of select="'gmi'"/>
    </xsl:when>

    <xsl:when test="$name='keycap'">
      <xsl:value-of select="'k'"/>
    </xsl:when>

    <xsl:when test="$name='keycombo'">
      <xsl:value-of select="'kc'"/>
    </xsl:when>

    <xsl:when test="$name='menuchoice'">
      <xsl:value-of select="'mch'"/>
    </xsl:when>

    <xsl:when test="$name='shortcut'">
      <xsl:value-of select="'shc'"/>
    </xsl:when>

    <!-- attributes -->

    <xsl:when test="$name='url'"> <!-- attr of ulink -->
      <xsl:value-of select="'href'"/>
    </xsl:when>

    <xsl:when test="$name='numeration'"> <!-- attr of orderedlist -->
      <xsl:value-of select="'type'"/>
    </xsl:when>

    <!-- if nothing matches, return the name itself -->

    <xsl:otherwise>
      <xsl:value-of select="$name"/>
    </xsl:otherwise>

  </xsl:choose>

</xsl:template>

</xsl:transform>
