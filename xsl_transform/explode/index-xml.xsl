<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Writes the file books/book_id/index.xml. It is called by explode.sh
like this:
  xsltproc -o index.xml \
           -stringparam id "book_id" -stringparam lang "lng" \
           index-xml.xsl book.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" version="1.0" encoding="utf-8" 
            omit-xml-declaration="no" standalone="no" indent="yes" />

<xsl:include href="../common/count.xsl" />
<xsl:include href="../common/node-path.xsl" />


<!-- handle books and articles -->
<xsl:template match="book | article">
  <xsl:copy>
    <xsl:attribute name="id">
      <xsl:value-of select="$id" />
    </xsl:attribute>
    <xsl:attribute name="lang">
      <xsl:value-of select="$lang" />
    </xsl:attribute>
    <xsl:attribute name="path">
      <xsl:value-of select="'./'" />
    </xsl:attribute>

    <xsl:apply-templates />
  </xsl:copy>
</xsl:template>


<!-- handle chapters and sections -->
<xsl:template match="bookinfo | articleinfo | preface | appendix
                    | chapter | section | simplesect">
  <xsl:copy>
    <xsl:attribute name="id">
      <xsl:apply-templates mode="getid" select="." />
    </xsl:attribute>
    <xsl:attribute name="path">
      <xsl:apply-templates mode="path" select="." />      
    </xsl:attribute>

    <xsl:apply-templates />
  </xsl:copy>
</xsl:template>


<!-- get the node id of chapters, sections, etc. -->
<xsl:template  mode="getid"
               match="preface | appendix | chapter | section | simplesect">
  <xsl:choose>
    <xsl:when test="@id">
      <xsl:value-of select="@id" />
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="count">
        <xsl:apply-templates select="." mode="count" />
      </xsl:variable> 
      <xsl:value-of select="concat(name(.), '-', $count)" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- always set id to INFO for bookinfo and articleinfo -->
<xsl:template  mode="getid" match="bookinfo | articleinfo">
  <xsl:value-of select="'INFO'" />
</xsl:template>


<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
